#! /usr/bin/env python

"""
This is a single-run script for quickly performing coupling function (CF) calculations.
It can only take in a single auxiliary channel and a single pair of background and injection times.
It is intentionally minimal in features and customizability.
It only requires gwpy and basic python libraries.
Although the full pemcoupling library can be used for simple, single-run analyses as well,
this is designed to be convenient for commissioners/PEM investigators who want to process a single injection
without having to worry about the bulky configuring process required to set up a pemcoupling run.

To utilize the full capabilities of the pemcoupling package
(many channels, many injections, many pre- and post-processing features),
install and use the pemcoupling executables by following instructions in the package documentation:
https://pemcoupling.readthedocs.io/en/stable/
"""

import time
import datetime
from argparse import ArgumentParser
import numpy as np
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt

from gwpy.time import from_gps, to_gps
from gwpy.timeseries import TimeSeries

# Fix usetex parameter changed by gwpy
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'


DEFAULT_DARM_CALIBRATION = None
DEFAULT_DURATION = 60
DEFAULT_FFTLENGTH = 10
DEFAULT_OVERLAP = 0.5
DEFAULT_FREQ_RANGE = (30, 2048)
DEFAULT_SENSOR_THRESHOLD = 10
DEFAULT_DARM_THRESHOLD = 2


parser = ArgumentParser()
parser.add_argument("filename", help="output text file, e.g. HAM5.txt")
parser.add_argument("channel", help="auxiliary channel name, e.g 'L1:PEM-CS_ACC_HAM5_SRM_DQ'")
parser.add_argument("darm_channel", help="e.g. L1:GDS-CALIB_STRAIN or L1:CAL-DELTAL_EXTERNAL_DQ")
parser.add_argument("t_bkg", type=float, help="background start time")
parser.add_argument("t_inj", type=float, help="injection start time")
parser.add_argument("--darm_calibration", default=DEFAULT_DARM_CALIBRATION,
                    help="text file w/ DARM calibration data, no calibration by default")
parser.add_argument("--duration", type=float, default=DEFAULT_DURATION,
                    help="duration in seconds, default = {}".format(DEFAULT_DURATION))
parser.add_argument("--fftlength", type=float, default=DEFAULT_FFTLENGTH,
                    help="FFT length in seconds, default = {}".format(DEFAULT_FFTLENGTH))
parser.add_argument("--overlap", type=float, default=DEFAULT_OVERLAP,
                    help="overlap fraction (0-1), default = {}".format(DEFAULT_OVERLAP))
parser.add_argument("--freq_range", type=float, nargs=2, default=DEFAULT_FREQ_RANGE,
                    help="frequency min & max values (takes two arguments, e.g. '--freq_range 10 300'), default = ({}, {})".format(*DEFAULT_FREQ_RANGE))
parser.add_argument("--sensor_threshold", type=float, default=DEFAULT_SENSOR_THRESHOLD,
                    help="min sensor ratio for setting upper limit or measuring coupling, default = {}".format(DEFAULT_SENSOR_THRESHOLD))
parser.add_argument("--darm_threshold", type=float, default=DEFAULT_DARM_THRESHOLD,
                    help="min DARM ratio for measuring coupling, default = {}".format(DEFAULT_DARM_THRESHOLD))
args = parser.parse_args()

# User parameters
filename = args.filename
channel = args.channel
darm_channel = args.darm_channel
darm_calibration = args.darm_calibration
t_bkg = args.t_bkg
t_inj = args.t_inj
duration = args.duration
fftlength = args.fftlength
overlap = args.overlap * fftlength
freq_range = args.freq_range
sensor_threshold = args.sensor_threshold
darm_threshold = args.darm_threshold

# Get time series
ts_sens_bkg = TimeSeries.get(channel, t_bkg, t_bkg + duration)
ts_sens_inj = TimeSeries.get(channel, t_inj, t_inj + duration)
ts_darm_bkg = TimeSeries.get(darm_channel, t_bkg, t_bkg + duration)
ts_darm_inj = TimeSeries.get(darm_channel, t_inj, t_inj + duration)

# Generate ASDs and crop to match freq range
sens_bkg = ts_sens_bkg.asd(fftlength, overlap)
sens_inj = ts_sens_inj.asd(fftlength, overlap)
darm_bkg = ts_darm_bkg.asd(fftlength, overlap)
darm_inj = ts_darm_inj.asd(fftlength, overlap)
if 'STRAIN' in darm_channel:
    darm_bkg *= 4000
    darm_inj *= 4000
sens_freqs = sens_bkg.frequencies.value
darm_freqs = darm_bkg.frequencies.value
fmin_ = max([freq_range[0], sens_freqs[0], darm_freqs[0]])
fmax_ = min([freq_range[1], sens_freqs[-1], darm_freqs[-1]])
sens_bkg = sens_bkg.crop(fmin_, fmax_)
sens_inj = sens_inj.crop(fmin_, fmax_)
darm_bkg = darm_bkg.crop(fmin_, fmax_)
darm_inj = darm_inj.crop(fmin_, fmax_)
freqs = sens_bkg.frequencies.value

# Apply DARM calibration file
if darm_calibration is not None:
    darm_cal_freqs, darm_cal_db, _ = np.loadtxt(darm_calibration).T
    darm_cal_factors = 10.0 ** (darm_cal_db / 20.)
    darm_cal_interp = np.interp(freqs, darm_cal_freqs, darm_cal_factors)
    darm_bkg.value[:] *= darm_cal_interp
    darm_inj.value[:] *= darm_cal_interp

# Determine frequencies of measurements vs upper limits
freq_mask = ((freqs >= freq_range[0]) & (freqs < freq_range[1]))
sens_ratio = np.ones_like(freqs)
darm_ratio = np.ones_like(freqs)
sens_bkg_nonzero = (sens_bkg.value > 0)
darm_bkg_nonzero = (darm_bkg.value > 0)
sens_ratio[sens_bkg_nonzero] = sens_inj.value[sens_bkg_nonzero] / sens_bkg.value[sens_bkg_nonzero]
darm_ratio[darm_bkg_nonzero] = darm_inj.value[darm_bkg_nonzero] / darm_bkg.value[darm_bkg_nonzero]
above_sens = (sens_ratio > sensor_threshold)
above_darm = (darm_ratio > darm_threshold)
measured = (freq_mask & above_sens & above_darm)
upperlim = (freq_mask & above_sens & ~above_darm)
belowthresh = (freq_mask & ~above_sens & ~above_darm)
nodata = ~(measured | upperlim | belowthresh)
# Numerator:
# Use difference in DARM if darm & sensor threshold passed
# Use DARM bkg otherwise
numer = np.zeros_like(freqs)
use_darm_diff = (measured | upperlim)
use_darm_inj = ~(use_darm_diff | nodata)
numer[use_darm_diff] = np.sqrt(
    darm_inj.value[use_darm_diff]**2 -
    darm_bkg.value[use_darm_diff]**2)
numer[use_darm_inj] = darm_inj.value[use_darm_inj]
# Denominator:
# Use difference in sensor if sensor threshold passed
# Use sensor bkg otherwise
denom = np.zeros_like(freqs)
use_sens_diff = (measured | upperlim)
use_sens_inj = ~(use_sens_diff | nodata)
denom[use_sens_diff] = np.sqrt(
    sens_inj.value[use_sens_diff]**2 -
    sens_bkg.value[use_sens_diff]**2)
denom[use_sens_inj] = sens_inj.value[use_sens_inj]
# Coupling function
cf = np.zeros_like(freqs)
denom_nonzero = (denom > 0)
cf[denom_nonzero] = numer[denom_nonzero] / denom[denom_nonzero]
cf[nodata] = 0
flags = np.array(['no data'] * len(freqs), dtype=object)
flags[measured] = 'Measured'
flags[upperlim] = 'Upper Limit'
flags[belowthresh] = 'Thresholds not met'

with open(filename, 'w') as file:
    file.write('freq,cf,flag\n')
    for i in range(len(freqs)):
        line = '{:.2f},{:.2e},{:s}'.format(freqs[i], cf[i], flags[i])
        file.write(line + '\n')


def plot_injection(data):
    """Export an estimated ambient plot from the data.
    """
    freq_min = data['freqs'][0]
    freq_max = data['freqs'][-1]
    # Y-AXIS LIMITS FOR SENSOR SPECTROGRAM
    freq_mask = (data['freqs'] >= freq_min) | (data['freqs'] <= freq_max)
    flag_mask = (data['flags'] == 'Measured') | (data['flags'] == 'Upper Limit')
    if flag_mask.sum() > 0:
        freq_mask = (freq_mask & flag_mask)
    else:
        freq_mask = freq_mask
    spec_sens_min = np.min(data['sens_bkg'][freq_mask] / 2)
    spec_sens_max = np.max(data['sens_inj'][freq_mask] * 2)
    # Y-AXIS LIMITS FOR DARM/EST AMB SPECTROGRAM
    amb_values = np.concatenate(
        (data['ambients'][freq_mask], data['darm_inj'][freq_mask]),
        axis=0)
    if np.any(amb_values):
        amb_min = np.min(amb_values[amb_values > 0])
        amb_max = np.max(amb_values[~np.isnan(amb_values)])
    else:
        amb_min = data['darm_bkg'].min() / 10.
        amb_max = data['darm_bkg'].max()
    spec_min = min([amb_min, min(data['darm_bkg']) / 10]) / 2
    spec_max = max([amb_max, max(data['darm_inj'])]) * 2
    # CREATE FIGURE FOR SPECTRUM PLOTS
    fig = plt.figure(figsize=(18, 9))
    # PLOT SENSOR SPECTRA
    ax1 = fig.add_subplot(211)
    ax1_x0 = 0.07
    ax1_x1 = 0.99
    ax1_y0 = 0.56
    ax1_y1 = 0.91
    ax1_pos = [ax1_x0, ax1_y0, ax1_x1 - ax1_x0, ax1_y1 - ax1_y0]
    ax1.set_position(ax1_pos)
    plt.plot(
        data['freqs'], data['sens_inj'], color='r', label='Injection', zorder=5)
    plt.plot(
        data['freqs'], data['sens_bkg'], color='k', label='Background', zorder=5)
    # CREATE LEGEND AND LABELS
    plt.legend()
    ylabel = "[Counts/sqrt(Hz)]".format()
    ylabel_size = 18
    plt.ylabel(ylabel, ha='center', va='center', labelpad=20, size=ylabel_size)
    plt.title(data['channel'], size=20)
    # CUSTOMIZE AXES
    plt.xlim([freq_min, freq_max])
    plt.ylim([spec_sens_min, spec_sens_max])
    if ((freq_max / freq_min) > 5):
        ax1.set_xscale('log')
    ax1.set_yscale('log')
    ax1.autoscale(False)
    plt.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    for tick in ax1.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax1.yaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    # PLOT DARM LINES
    ax2 = fig.add_subplot(212)
    ax2_x0 = 0.07
    ax2_x1 = 0.99
    ax2_y0 = 0.13
    ax2_y1 = 0.48
    ax2_pos = [ax2_x0, ax2_y0, ax2_x1 - ax2_x0, ax2_y1 - ax2_y0]
    ax2.set_position(ax2_pos)
    plt.plot(
        data['freqs'], data['darm_inj'], '-', color='r',
        label='DARM during injection', zorder=3)
    plt.plot(
        data['freqs'], data['darm_bkg'], '-', color='0.1',
        label='DARM background', zorder=4)
    # PLOT ESTIMATED AMBIENT
    # plot properties
    marker_measured, marker_upper = ('o', 'o')
    ms_measured, ms_upper = (3, 3)
    ew_measured, ew_upper = (0, 0)
    fc_measured, fc_upper = ('dodgerblue', 'grey')
    ec_measured, ec_upper = ('none', 'none')
    measured_amb = [[], []]
    upper_amb = [[], []]
    for y in range(len(data['freqs'])):
        if data['flags'][y] == 'Upper Limit':
            upper_amb[1].append(data['ambients'][y])
            upper_amb[0].append(data['freqs'][y])
        elif data['flags'][y] == 'Measured':
            measured_amb[1].append(data['ambients'][y])
            measured_amb[0].append(data['freqs'][y])
    plt.plot(
        measured_amb[0], measured_amb[1], marker_measured,
        markersize=ms_measured,
        markerfacecolor=fc_measured,
        markeredgecolor=ec_measured,
        markeredgewidth=ew_measured,
        label='Est. Amb.',
        zorder=6)
    plt.plot(
        upper_amb[0], upper_amb[1], marker_upper,
        markersize=ms_upper,
        markerfacecolor=fc_upper,
        markeredgecolor=ec_upper,
        markeredgewidth=ew_upper,
        label='Upper Limit Est. Amb.',
        zorder=5)
    # CREATE LEGEND AND LABELS
    legend = plt.legend(prop={'size': 12}, loc=1)
    legend.get_frame().set_alpha(0.5)
    plt.ylabel('DARM ASD [m/sqrt(Hz)]', size=18)
    plt.xlabel('Frequency [Hz]', size=18)
    plt.title('Estimated Ambient', size=20)
    # FIGURE TEXT
    str_inj_time = (
        'Injection Time: {}\n({})'
        .format(from_gps(data['t_inj']), data['t_inj']))
    str_quiet_time = (
        'Background Time: {}\n({})'
        .format(from_gps(data['t_bkg']), data['t_bkg']))
    plt.figtext(
        ax2_x0, 0.01, str_inj_time, ha='left', va='bottom', fontsize=12,
        color='b')
    plt.figtext(
        ax2_x1, 0.01, str_quiet_time, ha='right', va='bottom', fontsize=12,
        color='b')
    # CUSTOMIZE AXES
    plt.xlim([freq_min, freq_max])
    plt.ylim([spec_min, spec_max])
    if ((freq_max / freq_min) > 5):
        ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.autoscale(False)
    plt.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax2.yaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    return fig


data = dict(
    channel=channel,
    t_bkg=t_bkg,
    t_inj=t_inj,
    freqs=freqs,
    cf=cf,
    flags=flags,
    ambients=cf * sens_bkg.value,
    sens_bkg=sens_bkg.value,
    sens_inj=sens_inj.value,
    darm_bkg=darm_bkg.value,
    darm_inj=darm_inj.value)

plot = plot_injection(data)
plot.savefig(filename.replace('.txt', '.png'))
plt.show(block=True)