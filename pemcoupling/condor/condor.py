#!/usr/bin/python
"""
Parallelization of coupling function calculations.
"""

import os
import re
import glob
import subprocess
import shutil

from glue import pipeline

from gwpy.time import tconvert, from_gps

from .. import utils as pem_utils

# ===========================================================================

CONDOR_USER = os.getenv('USER')
CONDOR_GROUP = "ligo.prod.o3.detchar.explore.test"
TAG = 'pemcoupling'
UNIVERSE = 'vanilla'
REQUEST_MEMORY = 4096
RETRY = 0
for p in os.environ['PATH'].split(os.pathsep):
    if os.path.exists(p):
        if 'pemcoupling' in os.listdir(p):
            PEMCOUPLING = os.path.join(p, 'pemcoupling')
        if 'pemcoupling-composite' in os.listdir(p):
            PEMCOUPLING_COMPOSITE = os.path.join(p, 'pemcoupling-composite')

# ===========================================================================


def main(args):
    timestamp = from_gps(tconvert()).strftime('%Y-%m-%d_%H:%M:%S')
    # Handle arguments
    arg_dict = vars(args)
    del arg_dict['condor']
    arg_dict = {k.replace('_', '-'): v for k, v in arg_dict.items()}
    config = arg_dict.pop('config', None)
    sections = arg_dict.pop('section', None)
    out_dir = arg_dict.pop('output', None)
    if out_dir is None:
        out_dir = os.getcwd()
    dtt_files = arg_dict.pop('dtt')
    injection_list = arg_dict.pop('injection-list', None)
    search_injections = arg_dict.get('search-injections', None)
    composite = arg_dict.pop('composite', None)
    summary = arg_dict.pop('summary', None)
    # Create directories
    arg_dict = {k: v for k, v in arg_dict.items() if v is not None}
    job_dir = os.path.join(out_dir, 'jobs', timestamp)
    log_dir = os.path.join(job_dir, 'logs')
    temp_dir = os.path.join(job_dir, 'times')
    os.makedirs(temp_dir)
    for d in [out_dir, job_dir, log_dir, temp_dir]:
        if not os.path.exists(d):
            os.makedirs(d)
    # Get injection times, just like in pemcoupling.main()
    if dtt_files:
        print("Loading DTT files...")
        injection_table = []
        for dtt_file in dtt_files:
            injection_name = os.path.splitext(os.path.basename(dtt_file))[0]
            if pem_utils.search(search_injections, injection_name):
                try:
                    t_bkg, t_inj = pem_utils.get_times_from_DTT(dtt_file)
                except:
                    print("Failed to load DTT file {}".format(dtt_file))
                    continue
                injection_table.append([injection_name, t_bkg, t_inj])
    elif injection_list:
        print("Getting times from injection times file...")
        injection_table = pem_utils.get_times_from_txt(injection_list)
        if search_injections is not None:
            injection_table = [
                row for row in injection_table if
                pem_utils.search(search_injections, row[0])]
    else:
        raise ValueError("No times given.")
    if not injection_table:
        raise RuntimeError("No injection times found.")
    # Create a single-row injection list for each injection
    print("Injections to be analyzed:")
    injection_files = []
    for row in injection_table:
        injection_file = os.path.join(temp_dir, row[0] + '.txt')
        line = [row[0], str(row[1]), str(row[2])]
        line_str = ','.join(line)
        print(line_str)
        with open(injection_file, 'w') as f:
            f.write(line_str + '\n')
        injection_files.append(injection_file)
    print("Setting up Condor jobs...")
    # Initialize pipeline
    dag = pipeline.CondorDAG(os.path.join(log_dir, '{}.log'.format(TAG)))
    dag.set_dag_file(os.path.join(job_dir, TAG))
    dag_file = dag.get_dag_file()
    dag_file_basename = os.path.splitext(dag_file)[0]
    setup_sub_file = '{}_setup.sub'.format(dag_file_basename)
    analysis_sub_file = '{}_analysis.sub'.format(dag_file_basename)
    post_sub_file = '{}_post.sub'.format(dag_file_basename)
    cleanup_sub_file = '{}_cleanup.sub'.format(dag_file_basename)

    # Setup job to generate lock file
    lock_file = os.path.join(out_dir, 'lock.txt')
    setup_script = os.path.join(job_dir, 'setup.sh')
    with open(setup_script, 'w') as f:
        f.write("#! /usr/bin/sh\n")
        f.write("touch {lock_file}\n".format(lock_file=lock_file))
    os.chmod(setup_script, 0o777)
    setup_job = pipeline.CondorDAGJob(UNIVERSE, setup_script)
    setup_job.set_sub_file(setup_sub_file)
    setup_log_stub = os.path.join(log_dir, '{}-$(cluster)-$(process)'.format(TAG))
    setup_job.set_log_file('{}.log'.format(setup_log_stub))
    setup_job.set_stdout_file('{}.out'.format(setup_log_stub))
    setup_job.set_stderr_file('{}.err'.format(setup_log_stub))
    setup_job.add_condor_cmd('getenv', 'True')
    setup_job.add_condor_cmd('priority', 20)
    setup_job.add_condor_cmd('accounting_group', CONDOR_GROUP)
    setup_job.add_condor_cmd('accounting_group_user', CONDOR_USER)
    # Node for setup job
    setup_node = pipeline.CondorDAGNode(setup_job)
    setup_node.set_category('setup')
    setup_node.set_retry(RETRY)

    # Analysis job
    analysis_job = pipeline.CondorDAGJob(UNIVERSE, PEMCOUPLING)
    analysis_job.set_sub_file(analysis_sub_file)
    analysis_log_stub = os.path.join(log_dir, '{}-$(cluster)-$(process)'.format(TAG))
    analysis_job.set_log_file('{}.log'.format(analysis_log_stub))
    analysis_job.set_stdout_file('{}.out'.format(analysis_log_stub))
    analysis_job.set_stderr_file('{}.err'.format(analysis_log_stub))
    analysis_job.add_condor_cmd('getenv', 'True')
    analysis_job.add_condor_cmd('priority', 20)
    analysis_job.add_condor_cmd('accounting_group', CONDOR_GROUP)
    analysis_job.add_condor_cmd('accounting_group_user', CONDOR_USER)
    if UNIVERSE != 'local':
        analysis_job.add_condor_cmd('request_memory', REQUEST_MEMORY)
    # Add pemcoupling options
    analysis_job.add_arg('--')
    analysis_job.add_arg(config)
    for section in sections:
        analysis_job.add_arg(section)
    analysis_job.add_opt('output', out_dir)
    for arg, val in arg_dict.items():
        if val is not None and arg != 'search-injections':
            if type(val) is bool:
                if val:
                    analysis_job.add_opt(arg, None)
            elif type(val) is list:
                for v in val:
                    analysis_job.add_opt(arg, str(v))
            elif type(val) is tuple:
                analysis_job.add_opt(arg, ' '.join([str(o) for v in val]))
            else:
                analysis_job.add_opt(arg, str(val))
    # Node for each DTT or injection table row
    analysis_nodes = []
    for injection_file in injection_files:
        analysis_node = pipeline.CondorDAGNode(analysis_job)
        analysis_node.set_category('pemcoupling')
        analysis_node.set_retry(RETRY)
        analysis_node.set_log_file(injection_file.rstrip('.xml'))
        analysis_node.add_var_opt('injection-list', injection_file)
        analysis_nodes.append(analysis_node)

    # Post-processing job
    if composite:
        post_job = pipeline.CondorDAGJob(UNIVERSE, PEMCOUPLING_COMPOSITE)
        post_job.set_sub_file(post_sub_file)
        post_log_stub = os.path.join(
            log_dir, '{}-$(cluster)-$(process)'.format(TAG))
        post_job.set_log_file('{}.log'.format(post_log_stub))
        post_job.set_stdout_file('{}.out'.format(post_log_stub))
        post_job.set_stderr_file('{}.err'.format(post_log_stub))
        post_job.add_condor_cmd('getenv', 'True')
        post_job.add_condor_cmd('accounting_group', CONDOR_GROUP)
        post_job.add_condor_cmd('accounting_group_user', CONDOR_USER)
        post_job.add_arg(config)
        for section in sections:
            post_job.add_arg(section)
        post_job.add_opt('directory', out_dir)
        channels = arg_dict.get('channel', None)
        if channels:
            for c in channels:
                post_job.add_opt('-c', c.split(',')[0])
        opts = [
            'channel-list',
            'search-channels',
            'search-injections'
        ]
        for opt in opts:
            val = arg_dict.get(opt, None)
            if val:
                post_job.add_opt(opt, "'{}'".format(val))
        if summary:
            post_job.add_opt('summary', None)
        # Node for post-processing job
        post_node = pipeline.CondorDAGNode(post_job)
        post_node.set_category('pemcoupling_post')
        post_node.set_retry(RETRY)
    else:
        post_node = None

    # Cleanup job to remove lock file
    cleanup_script = os.path.join(job_dir, 'cleanup.sh')
    with open(cleanup_script, 'w') as f:
        f.write("#! /usr/bin/sh\n")
        f.write("rm {lock_file}\n".format(lock_file=lock_file))
    os.chmod(cleanup_script, 0o777)
    cleanup_job = pipeline.CondorDAGJob(UNIVERSE, cleanup_script)
    cleanup_job.set_sub_file(cleanup_sub_file)
    cleanup_log_stub = os.path.join(log_dir, '{}-$(cluster)-$(process)'.format(TAG))
    cleanup_job.set_log_file('{}.log'.format(cleanup_log_stub))
    cleanup_job.set_stdout_file('{}.out'.format(cleanup_log_stub))
    cleanup_job.set_stderr_file('{}.err'.format(cleanup_log_stub))
    cleanup_job.add_condor_cmd('getenv', 'True')
    cleanup_job.add_condor_cmd('priority', 20)
    cleanup_job.add_condor_cmd('accounting_group', CONDOR_GROUP)
    cleanup_job.add_condor_cmd('accounting_group_user', CONDOR_USER)
    # Node for cleanup job
    cleanup_node = pipeline.CondorDAGNode(cleanup_job)
    cleanup_node.set_category('cleanup')
    cleanup_node.set_retry(RETRY)

    # Set parent-child relationships
    if post_node is not None:
        for analysis_node in analysis_nodes:
            analysis_node.add_parent(setup_node)
            post_node.add_parent(analysis_node)
        cleanup_node.add_parent(post_node)
    else:
        for analysis_node in analysis_nodes:
            analysis_node.add_parent(setup_node)
            cleanup_node.add_parent(analysis_node)
        
    # Add nodes to DAG
    dag.add_node(setup_node)
    for analysis_node in analysis_nodes:
        dag.add_node(analysis_node)
    if post_node is not None:
        dag.add_node(post_node)
    dag.add_node(cleanup_node)

    # Write condor and DAG files and submit the DAG to condor
    dag.write_sub_files()
    dag.write_dag()
    print("Submitting Condor DAG " + dag_file)
    subprocess.call('condor_submit_dag ' + dag_file, shell=True)
    return
