import numpy as np
import time
import datetime
import os
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
from gwpy.timeseries import TimeSeries

# Fix usetex parameter changed by gwpy
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'


def coherence(
        gw_channel, ifo, ts_sensor_list,
        t_start, t_end, fft_time, overlap_time,
        cohere_plot, coherence_threshold, percentage_threshold,
        directory=None, timestamp=None):
    """
    Coherence between DARM signal and sensor signal. Notifies user if a certain
    amount of the data exhibits poor coherence. This coherence data will be
    reported in the csv output file.

    Parameters
    ----------
    gw_channel : str
        Either 'strain_channel' or 'deltal_channel'.
    ifo : str
        Interferometer name, 'H1' or 'L1'.
    ts_sensor_list : list
        Sensor gwpy.timeseries.TimeSeries objects.
    t_start : float, int
        Start time of time series segment.
    t_end : float, int
        End time of time series segment.
    fft_time :
        Length (seconds) of FFT averaging windows.
    overlap_time : float, int
        Overlap time (seconds) of FFT averaging windows.
    cohere_plot : bool
        If True, save coherence plot to path.
    coherence_threshold : float, int
        Coherence threshold. Used only for notifications.
    percentage_threshold : float, int
        Threshold for how much of coherence data falls below thresh1. Used only
        for notifications.
    directory : str
        Output directory
    timestamp : time.time object
        Time stamp of main routine; important for output plots and directory
        name.
s
    Returns
    -------
    coherence_dict : dict
        Dictionary of channel names and coherences (gwpy FrequencySeries
        object).
    """

    if timestamp is None:
        timestamp = time.time()
    timestamp_1 = time.time()
    coherence_dict = {}
    for i, ts_sensor in enumerate(ts_sensor_list):
        channel_name = ts_sensor.name
        # COMPUTE COHERENCE
        if gw_channel == 'strain_channel':
            ts_darm = TimeSeries.fetch(
                ifo + ':GDS-CALIB_STRAIN', t_start, t_end)
        elif gw_channel == 'deltal_channel':
            ts_darm = TimeSeries.fetch(
                ifo + ':CAL-DELTAL_EXTERNAL_DQ', t_start, t_end)
        coherence = ts_sensor.coherence(ts_darm, fft_time, overlap_time)
        values = np.asarray(coherence.value)
        freqs = np.asarray(coherence.frequencies.value)
        coherence_dict[channel_name] = values  # Save to output dictionary
        # PLOT COHERENCE DATA
        if cohere_plot:
            # Create plot
            ax = plt.subplot(111)
            plt.plot(freqs, values, '-', color='b')
            plt.ylabel('Coherence')
            plt.xlabel('Frequency [Hz]')
            plt.ylim([0, 1])
            plt.xlim([10, max(freqs)])
            ax.set_xscale('log', nonpositive='clip')
            ax.autoscale(False)
            plt.grid(
                b=True, which='major', color='0.75', linestyle=':', zorder=1)
            plt.minorticks_on()
            plt.grid(
                b=True, which='minor', color='0.65', linestyle=':', zorder=1)
            plt.title('%s and %s' % (channel_name[7:].rstrip('_DQ'),
                                     ts_darm.name[7:].rstrip('_DQ')))
            plt.suptitle(
                datetime.datetime.fromtimestamp(timestamp)
                .strftime('%Y-%m-%d %H:%M:%S'), fontsize=10)
            # Export plot
            if directory is None:
                directory = (datetime.datetime.fromtimestamp(timestamp)
                             .strftime('DATA_%Y-%m-%d_%H:%M:%S'))
            if not os.path.exists(directory):
                os.makedirs(directory)
            file_name = os.path.join(
                directory, '%s_coherence_plot.png' %
                channel_name[7:].rstrip('_DQ'))
            plt.savefig(file_name)
            plt.close()
        # COHERENCE REPORT
        # Reports coherence results if overall coherence falls below threshold
        pts_above_threshold = sum(values > coherence_threshold * 1e-2)
        pts_total = float(len(values))
        percent_above_threshold = (pts_above_threshold / pts_total)
        if percent_above_threshold < percentage_threshold * 1e-2:
            print("Less than {:.1f}% of {} data"
                  .format(percentage_threshold, channel_name) +
                  "has a coherence of {:.1f}% or greater"
                  .format(coherence_threshold))
            print("Only {:.3f}% of the data fit this criteria."
                  .format(percent_above_threshold))
        else:
            print("Coherence thresholds passed for channel {}."
                  .format(channel_name))
    print("Coherence(s) calculated. (Runtime: {:.3f} s)"
          .format(time.time() - timestamp_1))
    return coherence_dict
