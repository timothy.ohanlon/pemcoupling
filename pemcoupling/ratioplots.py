"""
Tools for generating ASD ratio plots for PEM injections.
"""

import numpy as np
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import os
import time

from gwpy.time import from_gps

# Fix usetex parameter changed by gwpy
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'


def get_ratio(asd_bg, asd_inj, method='raw'):
    """
    Get ratios between a channel's quiet and injected spectra.

    Parameters
    ----------
    asd_bg : `pemcoupling.pemchannel.PEMChannelASD` object
        Background (quiet) spectrum.
    asd_inj : `pemcoupling.pemchannel.PEMChannelASD` object
        Injection spectrum.
    method : {'raw', 'max', 'avg'}, optional
        Method for calculating ratio per frequency bin.
    """
    if not method in ['max', 'avg', 'raw']:
        raise ValueError(
            "Ratio plot method must be 'max', 'avg', or 'raw' (None).")
    if method == 'max' or method == 'avg':
        hz_idx = int(1. / (asd_inj.df))  # avg/max window size = 1 Hz
        freq = asd_inj.freqs[hz_idx: -1: hz_idx]
        num_pts = len(freq)
        ratio_raw = np.divide(asd_inj.values, asd_bg.values)
        if method == 'max':
            # Max ratio for each frequency bin
            ratio = np.zeros(num_pts)
            for i in range(num_pts):
                i1 = max([(i * hz_idx), 0])
                i2 = min([(i + 1) * hz_idx, len(ratio_raw)])
                ratio[i] = ratio_raw[i1: i2].max()
        elif method == 'avg':
            # Average ratio for each frequency bin
            ratio_conv = np.convolve(
                ratio_raw,
                np.ones(hz_idx) / hz_idx,
                mode='valid')
            ratio = ratio_conv[hz_idx: -1: hz_idx]
    else:
        # No binning, just get raw ratios
        freq = asd_inj.freqs
        ratio = np.divide(asd_inj.values, asd_bg.values)
    return ratio, freq


def ratio_plots(
        asd_bkg_list, asd_inj_list, ratio_min=None, ratio_max=None,
        method='raw', freq_min=None, freq_max=None,
        channels_per_plot=None, injection=None, filename=None,
        verbose=False):
    """
    Plots of injection-to-background ratios for each sensor, combined
    into tables.

    Parameters
    ----------
    asd_bkg_list : list
        Background sensor ASDs.
    asd_inj_list : list
        Injection sensor ASDs.
    ratio_min : float, int
        Minimum ratio for colorbar.
    ratio_max : float, int
        Maximum raito for colorbar.
    method : {'raw', 'max', 'avg}
        Show raw ('raw'), average ('avg'), or maximum ('max') ratio per
        frequency bin.
    freq_min : float, int, optional
        Minimum frequency for ratio plot.
    freq_max : float, int, optional
        Maximum frequency for ratio plot.
    channels_per_plot : int, optional
        Number of channels per plot. Default is 10.
    injection : {None, str}, optional
        Name of injection to be included in caption.
    filename : {None, str}, optional
        Base filename. If number of channels > channels_per_plot,
        then multiple .png files will be saved with numbers appended
        to filename.
    """

    timestamp = time.time()
    if verbose:
        print("Finding ratios using method {}.".format(method))
    if ratio_min is None:
        ratio_min = 10
    if ratio_max is None:
        ratio_max = 100
    if channels_per_plot is None:
        channels_per_plot = 10
    n_chans = len(asd_inj_list)
    ratios = []
    freqs = []  # List of frequency ranges for each channel
    for i in range(n_chans):
        ratio_1d, freq = get_ratio(
            asd_bkg_list[i], asd_inj_list[i], method=method)
        # Truncate values to ratio_min and ratio_max
        ratio_1d = np.maximum(ratio_1d, ratio_min)
        ratio_1d = np.minimum(ratio_1d, ratio_max)
        # matplotlib pcolor requires multidimensional arrays so we expand the
        # array of ratios to be 2-dimensional
        ratio_2d = np.expand_dims(ratio_1d, axis=0)
        ratios.append(ratio_2d)
        freqs.append(freq)
    if freq_min is None:
        freq_min = min([min(f) for f in freqs])
    if freq_max is None:
        freq_max = max([max(f) for f in freqs])

    # EXPORT DATA
    for start_idx in range(0, n_chans, channels_per_plot):
        figure_idx = (start_idx / channels_per_plot) + 1
        end_idx = min([start_idx + channels_per_plot, n_chans])
        n_chans_table = len(range(start_idx, end_idx))
        max_name_len = max(
            len(asd.name) for asd in asd_inj_list[start_idx:end_idx])
        fig_w = max_name_len / 2
        fig_h = n_chans_table + 2
        fontsize = 16
        font_h = (fontsize / 72.272) / fig_h
        plt.figure(figsize=(fig_w, fig_h))

        # MAIN PLOT LOOP
        for chan_idx in range(start_idx, end_idx):
            row_idx = chan_idx % channels_per_plot
            ax = plt.subplot2grid(
                (n_chans_table + 2, 20), (row_idx + 1, 6), colspan=13)
            cmap = plt.cm.gnuplot
            norm = LogNorm(vmin=ratio_min, vmax=ratio_max)
            im = ax.pcolor(
                freqs[chan_idx], np.array([0, 1]), ratios[chan_idx],
                cmap=cmap, vmin=ratio_min, vmax=ratio_max, norm=norm)
            ax.set_yticks([], [])
            ax.set_xlim([freq_min, freq_max])
            ax.set_xscale('log', nonpositive='clip')
            ax.xaxis.set_tick_params(which='major', direction='out', width=3)
            ax.xaxis.set_tick_params(which='minor', direction='out', width=2)
            ax.xaxis.grid(which='both', color='white', alpha=0.4)
            if not (row_idx == n_chans_table - 1):
                for xtick in ax.get_xticklabels():
                    xtick.set_label('')
            pos = ax.get_position()
            plt.figtext(
                0.02, pos.y0 + pos.height / 2.0 - font_h / 2.0,
                asd_inj_list[chan_idx].name, fontsize=fontsize,
                color='k', fontweight='bold')
        # Adds black tick labels to bottom plot
        for xtick in ax.get_xticklabels():
            xtick.set_color('k')

        # COLORBAR
        cax = plt.subplot2grid(
            (n_chans_table, 20), (int(n_chans_table / 2), 19), colspan=2)
        cbar = plt.colorbar(im, cax=cax)
        # Labels
        if ratio_max / ratio_min >= 10:
            tick_values = ratio_min * 2 ** np.arange(np.log2(ratio_max / ratio_min))
            if tick_values[-1] / ratio_max > 0.8:
                tick_values = tick_values[:-1]
            tick_values = np.append(tick_values, ratio_max)
        else:
            tick_values = np.linspace(ratio_min, ratio_max, 6)
        tick_labels = np.char.mod('%d', tick_values).astype('object')
        tick_labels[0] = '<' + tick_labels[0]
        tick_labels[-1] = '>' + tick_labels[-1]
        cbar.set_ticks(tick_values)
        cbar.ax.set_yticklabels(
            tick_labels, fontsize=(n_chans_table / 30. + 1) * fontsize)
        # Positioning
        cb_h = (0.23 * n_chans_table + 2) / float(fig_h)
        cax.set_position([0.9, 0.5 - cb_h / 2.0, 0.01, cb_h])

        # EXPORT
        plt.suptitle(
            "Ratio of Injected Spectra to Quiet Spectra",
            fontsize=fontsize * 2, color='k')
        t_bkg = int(asd_bkg_list[0].t0)
        t_inj = int(asd_inj_list[0].t0)
        t_bkg_utc = from_gps(t_bkg)
        t_inj_utc = from_gps(t_inj)
        if type(injection) is str:
            subtitle1 = "Injection: " + injection
        else:
            subtitle1 = ""
        subtitle2 = "Background: {} ({})".format(t_bkg_utc, t_bkg)
        subtitle3 = "Injection: {} ({})".format(t_inj_utc, t_bkg)
        plt.figtext(0.5, 0.08, subtitle1, fontsize=fontsize, color='b',
                    horizontalalignment='center', verticalalignment='bottom')
        plt.figtext(0.02, 0.02, subtitle2, fontsize=fontsize * 0.8,
                    horizontalalignment='left', verticalalignment='bottom')
        plt.figtext(0.98, 0.02, subtitle3, fontsize=fontsize * 0.8,
                    horizontalalignment='right', verticalalignment='bottom')
        if n_chans > channels_per_plot:
            fn = os.path.splitext(filename)[0] + '_{}.png'.format(figure_idx)
            plt.savefig(fn)
        else:
            plt.savefig(filename)
        plt.close()
    dt = time.time() - timestamp
    if verbose:
        print("Ratio tables complete. (Runtime: {:.3f} s)".format(dt))
    return
