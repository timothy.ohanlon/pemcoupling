"""
Site-wide coupling function calculator.
"""

import numpy as np
import pandas as pd
import sys
import os
import glob
import re
import logging

# pemcoupling modules
from ..parse import parse_config
from .. import utils as pem_utils
from . import sitewide_utils


def main(args):
    verbose = args.verbose

    # Find composite coupling functions
    if args.directory is None:
        directory = os.getcwd()
    else:
        directory = args.directory

    # Config parsing
    config_dict = parse_config(args.config)
    sect_cfg = config_dict[args.section[0]]
    comp_cfg = config_dict['composite coupling function']
    if sect_cfg['peak_search_window'] is not None:
        peak_search_window = max(sect_cfg['peak_search_window'], 0)
    else:
        peak_search_window = 0
    quad_sum = sect_cfg['quad_sum']
    if args.directory is not None:
        directory = args.directory
    else:
        directory = comp_cfg.get('composite_dir', os.getcwd())
    if args.output is not None:
        out_dir = args.output
    else:
        out_dir = os.path.join(directory, comp_cfg.get('summary_dir', 'SummaryCouplingFunctions'))
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Check for coupling function files
    glob_files = glob.glob(
        os.path.join(directory, '*_composite_coupling_data.txt'))
    if len(glob_files) == 0:
        logging.error(
            "No composite coupling functions directory " + directory +
            "\nMake sure you are in a directory that contains composite "
            "coupling functions or use the -d option to point to one.")
        sys.exit()
    cf_files = {}
    for file in glob_files:
        channel = (
            os.path.basename(file).rstrip('_composite_coupling_data.txt'))
        file_ifo, _, file_station, _ = pem_utils.parse_channel_name(channel)
        if args.station is not None:
            if file_station == args.station:
                cf_files[channel] = file
                logging.info("Including data from " + file)
        else:
            cf_files[channel] = file
            logging.info("Including data from " + file)

    # Search for channels
    if len(args.channel) > 0:
        channels_from_args = args.channel
        if quad_sum:
            qsum_dict = pem_utils.quad_sum_names(args.channel)
            channels_from_args += qsum_dict.keys()
        cf_files = {
            channel: filenames for channel, filenames in cf_files.items()
            if channel in channels_from_args}
    elif args.channel_list is not None:
        channels_from_cl = pem_utils.get_channels(
            args.channel_list, station=args.station)
        if quad_sum:
            qsum_dict = pem_utils.quad_sum_names(channels_from_cl)
            channels_from_cl += qsum_dict.keys()
        cf_files = {
            channel: filenames for channel, filenames in cf_files.items()
            if channel in channels_from_cl}
    cf_files_search = pem_utils.findall(
        args.search_channels, cf_files.keys())
    if len(cf_files_search) == 0:
        raise ValueError("No channels found matching search entry")
    cf_files = {k: cf_files[k] for k in cf_files_search}
    if len(cf_files) == 0:
        err_msg = "No channels to process."
        logging.error(err_msg)
        raise RuntimeError(err_msg)
    channels = sorted(cf_files.keys())
    print("\n{} channels(s) found:".format(len(channels)))
    for c in channels:
        print(c)
    print("")

    # Get ifo and station from channels or args
    ifo, station, summary_type = sitewide_utils.get_params_from_channels(channels)
    if ifo is not None:
        print("Ifo inferred from channels: " + ifo)
    if args.station is not None:
        station = args.station
    if station is not None:
        print("Station inferred from channels: " + station)

    # OTHER OPTIONS
    measured_value_threshold = args.measured_value_threshold
    fig_width = comp_cfg['fig_width']
    fig_height = comp_cfg['fig_height']
    upper_lim = (not args.no_upperlim)
    freq_range = args.freq_range
    cf_range = args.cf_range
    amb_range = args.amb_range
    if freq_range is None:
        x_min = comp_cfg['freq_min']
        x_max = comp_cfg['freq_max']
    else:
        x_min, x_max = freq_range
    if cf_range is None:
        cf_y_min = comp_cfg['coup_min']
        cf_y_max = comp_cfg['coup_max']
    else:
        cf_y_min, cf_y_max = cf_range
    if amb_range is None:
        amb_y_min = comp_cfg['amb_min']
        amb_y_max = comp_cfg['amb_max']
    else:
        amb_y_min, amb_y_max = amb_range
    if args.fig_size is not None:
        comp_cfg['fig_width'] = args.fig_size[0]
        comp_cfg['fig_height'] = args.fig_size[1]
    cf_title = args.cf_title
    amb_title = args.amb_title
    basename = args.basename
    gw_signal = comp_cfg['gw_signal']
    gwinc_label = comp_cfg['gwinc_label']
    gwinc_file = comp_cfg['gwinc_file']

    # ===========
    # DATA IMPORT
    # ===========

    # Import files as Pandas Dataframes
    print("Importing composite coupling functions...")
    bw_list = []
    data_list = []
    channels = []
    freq_lines = {}
    freqs_raw = []
    for c, f in cf_files.items():
        # Import file
        # Read header
        comb_freqs = []
        with open(f, 'r') as f_obj:
            for line in f_obj:
                if line[0] == '#':
                    match = re.search(
                        '#combfreqs:([\d.,]+)', line)
                    if match:
                        comb_freqs_str = match.group(1).split(',')
                        for s in comb_freqs_str:
                            comb_freqs.append(float(s))
                else:
                    break
        try:
            df = pd.read_csv(f, comment='#')
        except (IOError, OSError):
            print("No lowest coupling data found for " + c)
            continue
        # Crop to desired frequency range
        if freq_range is not None:
            df = df[(df['frequency'] >= freq_range[0])
                    & (df['frequency'] < freq_range[1])]
            df.reset_index(drop=True, inplace=True)
        # Save raw raw frequencies and darm for plotting later
        if df.shape[0] > len(freqs_raw):
            freqs_raw = df['frequency'].values
        bw_list.append(df['frequency'].iloc[10] - df['frequency'].iloc[9])
        data_list.append(df)
        channels.append(c)
        freq_lines[c] = comb_freqs
    short_channels = []
    for c in channels:
        if '-' in c:
            short_channels.append(c[c.index('-') + 1:])
        else:
            short_channels.append(c)
    # CHECK BANDWIDTHS
    bw = bw_list[0]
    if not all([bw_ == bw for bw_ in bw_list]):
        for i in range(len(channels)):
            print("{} {:.10e}".format(channels[i], bw_list[i]))
        print("\nError: Unequal bandwidths.\n")
        sys.exit()
    # CHECK DATA-FRAME SIZES
    if not all([i.shape[0] == data_list[0].shape[0] for i in data_list]):
        for i, df in enumerate(data_list):
            if df.shape[0] < len(freqs_raw):
                df_zeros = pd.DataFrame(
                    index=range(df.shape[0], len(freqs_raw)),
                    columns=df.columns)
                df = df.append(df_zeros)
                df['frequency'] = freqs_raw
                data_list[i] = df
    # RE-ORGANIZE DATAFRAMES
    # One dataframe per variable, with channels as columns
    df_index = np.array(range(data_list[0].shape[0]))
    factor_df = pd.DataFrame(index=df_index)
    flag_df = pd.DataFrame(index=df_index)
    amb_df = pd.DataFrame(index=df_index)
    darm_df = pd.DataFrame(index=df_index)
    for c, df in zip(*(channels, data_list)):
        factor_df[c] = df['factor']
        flag_df[c] = df['flag']
        amb_df[c] = df['ambient']
        darm_df[c] = df['darm']
    if verbose:
        print("Coupling functions imported.")
    # FREQUENCIES AND AVERAGE DARM FOR PLOTTING DARM BACKGROUND
    freqs = data_list[0]['frequency'].values
    darm_median = darm_df.apply(
        lambda x: np.median(x[~np.isnan(x)]), axis=1)
    darm_data = [freqs, darm_median]
    # GWINC (GW Inteferometer Noise Calculator) DARM ASD
    logging.info('Importing GWINC data.')
    try:
        gwinc_data = np.loadtxt(gwinc_file, unpack=True)
    except (IOError, OSError):
        logging.warning("Failed to read GWINC file " + gwinc_file)
        gwinc_data = None

    # ==========================
    # ANALYSIS // DATA EXPORT
    # ==========================
    
    # COMPUTE SUMMARY DATA
    print("Determining maximum coupling function...")
    max_factor_df = sitewide_utils.max_coupling_function(
        freqs, factor_df, flag_df, freq_lines=freq_lines,
        peak_search_window=peak_search_window)
    print("Determining maximum estimated ambient...")
    max_amb_df = sitewide_utils.max_estimated_ambient(
        freqs, amb_df, flag_df, freq_lines=freq_lines,
        peak_search_window=peak_search_window,
        measured_value_threshold=measured_value_threshold)
    # GENERATE EXPORT FILE NAMES
    if basename is None:
        if ifo is not None and station is not None:
            basename = '_'.join((ifo, station, summary_type))
        elif ifo is not None:
            basename = '_'.join((ifo, summary_type))
        elif station is not None:
            basename = '_'.join((station, summary_type))
        else:
            basename = summary_type
    filepath = os.path.join(out_dir, basename)
    csv_coupling_filepath = filepath + '_coupling_data.txt'
    csv_ambient_filepath = filepath + '_ambient_data.txt'
    plot_filepath_cf = filepath + '_coupling_plot.png'
    plot_filepath_amb = filepath + '_ambient_plot.png'
    plot_filepath_cf_multi = filepath + '_coupling_multiplot.png'
    plot_filepath_amb_multi = filepath + '_ambient_multiplot.png'
    if cf_title is None:
        cf_title = sitewide_utils.summary_plot_title(
            ifo, None, summary_type, plot='coupling')
    if amb_title is None:
        amb_title = sitewide_utils.summary_plot_title(
            ifo, None, summary_type, plot='ambient')
    # EXPORT CSV
    print("Saving to CSV...")
    cols_factor = ['frequency', 'factor', 'flag', 'channel']
    cols_amb = ['frequency', 'amb', 'flag', 'channel']
    max_factor_df[cols_factor].to_csv(
        csv_coupling_filepath, index=False)
    max_amb_df[cols_amb].to_csv(
        csv_ambient_filepath, index=False)
    # COUPLING FUNCTION PLOT
    print("Plotting coupling function...")
    sitewide_utils.plot_summary_coupfunc(
        max_factor_df,
        plot_filepath_cf,
        split_channels=False,
        upper_lim=upper_lim,
        bw=bw,
        x_min=x_min,
        x_max=x_max,
        y_min=cf_y_min,
        y_max=cf_y_max,
        title=cf_title,
        plot_style=args.plot_style,
        fig_width=fig_width,
        fig_height=fig_height,
        markers=comp_cfg['markers'],
        markersizes=comp_cfg['markersizes'],
        edgewidths=comp_cfg['edgewidths'],
        facecolors=comp_cfg['facecolors'])
    sitewide_utils.plot_summary_coupfunc(
        max_factor_df,
        plot_filepath_cf_multi,
        split_channels=True,
        upper_lim=upper_lim,
        bw=bw,
        x_min=x_min,
        x_max=x_max,
        y_min=cf_y_min,
        y_max=cf_y_max,
        title=cf_title,
        plot_style=args.plot_style,
        fig_width=fig_width,
        fig_height=fig_height,
        markers=comp_cfg['split_markers'],
        markersizes=comp_cfg['split_markersizes'],
        edgewidths=comp_cfg['split_edgewidths'])
    # ESTIMATED AMBIENT PLOT
    print("Plotting estimated ambient...")
    sitewide_utils.plot_summary_ambient(
        max_amb_df,
        plot_filepath_amb,
        split_channels=False,
        upper_lim=upper_lim,
        gw_signal=gw_signal,
        darm_data=darm_data,
        gwinc_data=gwinc_data,
        gwinc_label=gwinc_label,
        bw=bw,
        x_min=x_min,
        x_max=x_max,
        y_min=amb_y_min,
        y_max=amb_y_max,
        title=amb_title,
        plot_style=args.plot_style,
        fig_width=fig_width,
        fig_height=fig_height,
        markers=comp_cfg['markers'],
        markersizes=comp_cfg['markersizes'],
        edgewidths=comp_cfg['edgewidths'],
        facecolors=comp_cfg['facecolors'])
    sitewide_utils.plot_summary_ambient(
        max_amb_df,
        plot_filepath_amb_multi,
        split_channels=True,
        upper_lim=upper_lim,
        gw_signal=gw_signal,
        darm_data=darm_data,
        gwinc_data=gwinc_data,
        gwinc_label=gwinc_label,
        bw=bw,
        x_min=x_min,
        x_max=x_max,
        y_min=amb_y_min,
        y_max=amb_y_max,
        title=amb_title,
        plot_style=args.plot_style,
        fig_width=fig_width,
        fig_height=fig_height,
        markers=comp_cfg['split_markers'],
        markersizes=comp_cfg['split_markersizes'],
        edgewidths=comp_cfg['split_edgewidths'])
    print("Program has finished.")
    return
