"""pemcoupling utils"""

import numpy as np
import re
import glob
import os
import logging
import datetime
import time
from xml.etree import ElementTree

from gwpy.time import to_gps

from .pemchannel import PEMChannel


def configure_logger(log_dir):
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    time_now = datetime.datetime.fromtimestamp(time.time())
    time_str = time_now.strftime('%Y_%b_%d_%H:%M:%S')
    logging_filename = os.path.join(log_dir,  '{}.log'.format(time_str))
    logging.basicConfig(
        filename=logging_filename,
        level=logging.DEBUG,
        format='%(asctime)s %(filename)s (%(funcName)s) ' +
        '%(levelname)s: %(message)s',
        datefmt='%H:%M:%S')
    stderrLogger = logging.StreamHandler()
    stderrLogger.setLevel(logging.WARNING)
    stderr_formatter = logging.Formatter('%(levelname)s: %(message)s')
    stderrLogger.setFormatter(stderr_formatter)
    logging.getLogger().addHandler(stderrLogger)


def get_channels(
        filename, return_calibration_factors=False, ifo=None,
        station=None):
    """
    Get list of channels from a .txt file.

    Parameters
    ----------
    filename : str
        Name of file containing channel list.
    ifo : str
        Interferometer name, 'H1' or 'L1'.
    station : {'ALL', 'CS', 'EX', 'EY'} optional
        CS, EX, EY, or ALL (default ALL).

    Returns
    -------
    channels : list of strings
        Full channel names, e.g. 'H1:PEM-CS_ACC_BEAMTUBE_MCTUBE_Y_DQ'.
    """

    logging.info("Opening channel list file.")
    try:
        with open(filename.replace(' ', ''), 'r') as file:
            lines = file.readlines()
    except (IOError, OSError):
        logging.error("Channel list {} not found.".format(filename))
        return []
    # Read and sort channel names
    channels = []
    calibration_factors = {}
    calibration_units = {}
    for line in lines:
        if 'channel,calibration,unit\n' in line:
            continue
        split = line.strip().split()
        if len(split) > 0:
            channel = PEMChannel(split[0])
        else:
            continue
        if len(split) > 1:
            try:
                calibration = float(split[1])
            except (ValueError, TypeError):
                calibration = 1
        else:
            calibration = 1
        if len(split) > 2:
            unit = split[2]
        else:
            unit = 'Counts'
        match_ifo, match_station = (False, False)
        if ifo in ['H1', 'L1'] and channel.ifo == ifo:
            match_ifo = True
        else:
            match_ifo = True
        if station in ['CS', 'EX', 'EY']:
            if ((channel.station == station) or
                    (station == 'CS' and channel.system == 'IMC')):
                match_station = True
        else:
            match_station = True
        if match_ifo and match_station:
            channels.append(channel.name)
            calibration_factors[channel.name] = calibration
            calibration_units[channel.name] = unit
    if len(channels) == 0:
        raise ValueError("No channels found in " + filename)
    channels.sort()
    if return_calibration_factors:
        return channels, calibration_factors, calibration_units
    else:
        return channels


def get_calibration_factors(filename):
    _, out = get_channels(
        filename, return_calibration_factors=True)
    return out


def get_times_from_args(times):
    try:
        t_bkg = int(to_gps(times[0].strip()))
        t_inj = int(to_gps(times[1].strip()))
    except:
        err_msg = "Invalid GPS or UTC times for --times."
        raise OSError(err_msg)
    else:
        return t_bkg, t_inj


def get_DTT_directory(dtt_dir):
    if not os.path.exists(dtt_dir):
        raise OSError(
            "DTT directory {} does not exist.".format(dtt_dir))
    dtt_names = glob.glob(os.path.join(dtt_dir, '*.xml'))
    return dtt_names


def get_times_from_DTT(filename):
    """
    Get injection names and times from a single .xml file (saved from DTT).

    Parameters
    ----------
    filename : str
        DTT file name (.xml).

    Returns
    -------
    t_bkg : int
        GPS time of earliest reference time.
    t_inj : int
        GPS time of latest measurement time.
    """
    try:
        tree = ElementTree.parse(filename)
    except IOError:
        raise IOError("DTT file {} not found.".format(filename))
    except ElementTree.ParseError:
        reference_times, result_times = get_times_from_DTT_raw(filename)
        if len(result_times) == 0:
            raise ElementTree.ParseError(
                "Could not parse {} as a DTT file.".format(filename))
    else:
        root = tree.getroot()
        reference_times = []
        result_times = []
        for child in root:
            try:
                name = child.attrib['Name']
            except (AttributeError, KeyError):
                continue
            times_str = child.findall('Time')
            times = []
            for t in times_str:
                try:
                    times.append(int(float(t.text)))
                except (ValueError, AttributeError):
                    continue
            if re.search('Reference.*', name):
                reference_times += times
            elif re.search('Result.*', name):
                result_times += times
    if len(reference_times) == 0:
        raise ValueError(
            "No reference times found in {}".format(filename))
    if len(result_times) == 0:
        raise ValueError(
            "No measurement times found in {}".format(filename))
    t_bkg = min([t for t in reference_times if t > 0])
    t_inj = max(result_times)
    return t_bkg, t_inj


def get_times_from_DTT_raw(filename):
    """Get times from DTT file without xml parsing.

    Parameters
    ----------
    filename : str
        DTT file name (.xml).

    Returns
    -------
    background_times : list
        GPS time of earliest reference time.
    injection_times : list
        GPS time of latest measurement time.
    """
    background_times = []
    injection_times = []
    with open(filename) as DTT_file:
        lines = DTT_file.readlines()
    # Main loop for getting times
    times = {'Start': [], 'Time': [], 't0': [], 'TestTime': []}
    for i, line in enumerate(lines):
        line = line.replace(' ', '').replace('\n', '')
        # Add time to times dictionary if a time is found in this line
        if '"GPS"' in line:
            tname = line[
                line.index('TimeName') + 10: line.index('Type') - 1]
            t = line[line.index('"GPS"') + 6: line.index('</Time>')]
            t = int(float(t))
            if (t not in times[tname]):
                times[tname].append(t)
        if 'TestTime' not in times.keys() or 't0' not in times.keys():
            logging.warning(
                "Missing background or injection time found in file " +
                filename)
        else:
            t0_list = [
                t0 for t0 in times['t0'] if (t0 not in injection_times)]
            if len(times['TestTime']) == 0 or len(t0_list) == 0:
                logging.warning(
                    "Missing background or injection time found in file " +
                    filename)
            else:
                time_inj = max(times['TestTime'])
                injection_times.append(time_inj)
                time_bg = max(
                    [t0 for t0 in times['t0'] if (t0 not in injection_times)])
                background_times.append(time_bg)
                injection_times.append(time_inj)
    return background_times, injection_times



def get_times_from_txt(filename):
    """
    Get injection names and times from a .txt table. The table must be formatted
    as three (comma-separated) columns: injection name, background time, and
    injection time.

    Parameters
    ----------
    filename : str
        Name of .txt file.

    Returns
    -------
    injection_table : list
        Injection table as a list of lists.
    """

    injection_table = []
    try:
        with open(filename) as file:
            lines = file.readlines()
    except (IOError, OSError):
        err_msg = "Injection names/times file {} not found.".format(filename)
        logging.error(err_msg)
        raise OSError(err_msg)
    for i, line in enumerate(lines):
        row = line.split(',')
        try:
            name = row[0].strip()
            t_bkg = int(to_gps(row[1].strip()))
            t_inj = int(to_gps(row[2].strip()))
            injection_table.append([name, t_bkg, t_inj])
        except (IndexError, ValueError):
            logging.warning(
                "Could not parse line {} in {}".format(i, filename))
    return injection_table


def get_smooth_params_from_cfg(smooth_cfg, channels):
    smoothing_log = smooth_cfg['smoothing_log']
    smooth_dict = {}
    for channel in channels:
        smooth_dict[channel] = None
        for option, values in smooth_cfg.items():
            matches = re.findall('(.*)_smoothing', option)
            if matches:
                if ((matches[0].upper() in channel)
                        or (matches[0].upper() == 'ALL')):
                    smooth_dict[channel] = (
                        values[0], values[1], smoothing_log)
                    break
            elif option == 'smoothing':
                smooth_dict[channel] = (
                    values[0], values[1], smoothing_log)
                break
    return smooth_dict


def parse_injection_name(injection_name, injection_type, verbose=False):
    """
    Get fundamental frequency of an injection if is a magnetic injection.

    Parameters
    ----------
    injection_name : str
    injection_type : {broadband, comb, sine}

    Returns
    -------
    out : float
        Fundamental frequency (Hz).
    """

    if injection_type == 'broadband':
        pattern = '(\d+p*\d*k*)_*to_*(\d+p*\d*k*)_*hz'
        search = re.findall(pattern, injection_name, re.IGNORECASE)
        if search:
            pair_str = search[0]
            pair = (
                float(pair_str[0].lower().replace('p', '.').replace('k', 'e3')),
                float(pair_str[1].lower().replace('p', '.').replace('k', 'e3'))
            )
            return pair
        else:
            return None
    elif injection_type == 'comb':
        # Search for either one of two patterns:
        # (1) 'p' between two numbers (eg 7p1)
        # (2) number followed by 'Hz' (eg 7Hz)
        pattern = '([0-9]+p[0-9]+)|([0-9]+Hz)'
        search = re.search(pattern, injection_name, re.IGNORECASE)
        if search:
            # If found, get fundamental frequency from search result
            freq_str = search.group(0)
            freq_str = re.sub('Hz', '', freq_str, flags=re.IGNORECASE)
            freq_str = re.sub('p', '.', freq_str, flags=re.IGNORECASE)
            logging.info("Fundamental frequency found: %s Hz." % freq_str)
            if verbose:
                print("Fundamental frequency found: %s Hz." % freq_str)
                print("Coupling will only be computed near harmonics " +
                      "of this frequency.")
            return float(freq_str)
        else:
            return None


def nonzero_divide(y1, y2, ymin=0):
    """
    Nonzero division of two arrays.
    """

    np.seterr(divide='ignore')
    out = np.divide(y1, y2)
    out = np.maximum(ymin, out)
    return out


def pem_sort(chans):
    """
    Sort PEM channels by location.

    Parameters
    ----------
    chans : list
        Channel names to be sorted.

    Returns
    -------
    chans_sorted : list
        Sorted channel names.
    """

    sort_key = [
        'PSL',
        'ISCT1',
        'IOT2',
        'HAM1',
        'HAM2',
        'INPUT',
        'MCTUBE',
        'HAM3',
        'LVEA_BS',
        'CS_ACC_BSC',
        'VERTEX',
        'OPLEV_ITMY',
        'YMAN',
        'YCRYO',
        'OPLEV_ITMX',
        'XMAN',
        'XCRYO',
        'HAM4',
        'SRTUBE',
        'HAM5',
        'HAM6',
        'OUTPUTOPTICS',
        'ISCT6',
        'SQZT6',
        'CS_ACC_EBAY',
        'CS_MIC_EBAY',
        'CS_',
        'EX_',
        'EY_'
    ]
    chans_sorted = []
    for key in sort_key:
        # Add channels that match this key and are not yet added
        chans_sorted += sorted(
            [c for c in chans if (key in c) and (c not in chans_sorted)])
    # Add remaining channels at the end
    chans_sorted += sorted([c for c in chans if (c not in chans_sorted)])
    return chans_sorted


def quad_sum_names(channel_names):
    """
    Find tri-axial channel names and make corresponding quadrature-summed
    channel names.

    Parameters
    ----------
    channel_names : list of strings

    Returns
    -------
    qsum_dict : dict
        Keys are quad-sum channel names; values are lists of X, Y, and
        Z channel names.
    """

    mag_pattern = '.*PEM-.*_MAG_.*_(X|Y|Z)$'
    wfs_pattern = '.*IMC-WFS_.*(_PIT|_YAW)_OUT$'
    qsum_dict = {}
    for name in channel_names:
        mag_match = re.search(mag_pattern, name)
        wfs_match = re.search(wfs_pattern, name)
        if mag_match is not None or wfs_match is not None:
            if mag_match is not None:
                qsum_name = name[:-1] + 'XYZ'
            elif wfs_match is not None:
                if '_PIT_' in name:
                    qsum_name = name.replace(
                        '_PIT_OUT', '_PITYAW_QUADSUM')
                else:
                    qsum_name = name.replace(
                        '_YAW_OUT', '_PITYAW_QUADSUM')
            if qsum_name not in qsum_dict.keys():
                qsum_dict[qsum_name] = [name]
            else:
                qsum_dict[qsum_name].append(name)
    qsum_dict = {
        name: axes for name, axes in qsum_dict.items() if len(axes) > 1}
    return qsum_dict


def findall(pattern, strings):
    return [s for s in strings if search(pattern, s)]


def search(pattern, string):
    if pattern is None:
        return True
    pattern = pattern.strip()
    if ' ' in pattern:
        pattern = pattern.upper()
        split_or = re.split(' OR ', pattern)
        match_or = []
        for pattern_or in split_or:
            split_and = re.split (' AND ', pattern_or)
            match_and = []
            for pattern_and in split_and:
                if 'NOT ' in pattern_and:
                    pattern_not = pattern_and.lstrip('NOT ')
                    match_and.append(
                        pattern_not.lower() not in string.lower())
                else:
                    match_and.append(
                        pattern_and.lower() in string.lower())
            match_or.append(all(match_and))
        match = any(match_or)
    else:
        match = bool(re.search(pattern, string, re.IGNORECASE))
    return match


def parse_channel_name(channel):
    pattern = '(.*):(.*)-([^_]*)_([^_]*)'
    try:
        ifo, subsystem, station, sensor = re.findall(pattern, channel)[0]
    except IndexError:
        raise IndexError("Could not parse channel name " + channel)
    if station == 'WFS':
        sensor = 'WFS'
    if subsystem == 'IMC':
        station = 'CS'
    return ifo, subsystem, station, sensor


def near_comb(comb_freq, freqs, delta_f):
    f_mod = freqs % comb_freq
    prox = np.column_stack((f_mod, comb_freq - f_mod)).min(axis=1)
    out = prox < (0.5 * delta_f)
    return out


def near_line(line_freq, freqs, delta_f):
    return np.abs(freqs - line_freq) < (0.5 * delta_f)


def in_freq_band(band, freqs, pad=0):
    fmin = (1.0 - pad) * band[0]
    fmax = (1.0 + pad) * band[1]
    out = ((freqs > fmin) & (freqs < fmax))
    return out


def asd_slope(asd, window):
    slope = np.ones_like(asd)
    for i in range(1, slope.size - 1):
        lo = max(0, i - int(window * i))
        hi = min(slope.size, i + int(window * i))
        avg_below = asd[lo:i].mean()
        avg_above = asd[i:hi].mean()
        slope[i] = avg_above / avg_below
    return slope
