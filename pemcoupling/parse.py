import logging
import sys
import os
from argparse import ArgumentParser
try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser

from . import utils as pem_utils
from .pemchannel import PEMChannel


FILENAME_OPTIONS = [
    'channel_list',
    'calibration',
    'gwinc_file'
]
PLOT_OPTIONS = [
    'markers',
    'markersizes',
    'edgewidths',
    'facecolors',
    'edgecolors',
    'split_markers',
    'split_markersizes',
    'split_edgewidths'
]
FLOAT_OPTIONS = [
    'darm_threshold',
    'sensor_threshold',
    'sensor_threshold_autoscale',
    'peak_search_window',
    'duration',
    'overlap_pct',
    'bin_width',
    'broadband_freqs',
    'comb_freq',
    'line_freq',
]
INT_OPTIONS = [
    'fft_avg',
    'channels_per_plot',
    'coup_fig_width',
    'coup_fig_height',
    'spec_fig_width',
    'spec_fig_height',
    'fig_width',
    'fig_height'
]
BOOL_OPTIONS = [
    'ignore_saturated',
    'smoothing_log',
    'quad_sum',
    'auto_broadband_freqs',
    'auto_comb_freqs',
    'darm/10',
    'plot'
]
FLOAT_REQUIRED = [
    'darm_threshold',
    'sensor_threshold'
]
INT_REQUIRED = [
    'ratio_num_channels'
]


def parse_args(script, args=None):
    """
    All-in-one argument parser for pemcoupling scripts.
    """

    parser = ArgumentParser(
        add_help=False,
        epilog="Example: pemcoupling config-lho.ini ACC --station CS -t 1234567000 1234568000")
    parser.add_argument(
        "config", help="configuration file")
    parser.add_argument(
        "section", nargs="+", help="config file section(s) to process")

    # Define argument groups
    top_args = parser.add_argument_group("top-level options")
    channel_args = parser.add_argument_group("channel selection options")
    injection_args = parser.add_argument_group("injection options")
    fft_args = parser.add_argument_group("fft options")
    analysis_args = parser.add_argument_group("analysis options")
    plot_args = parser.add_argument_group("plotting options")
    postproc_args = parser.add_argument_group("post-processing options")

    top_args.add_argument(
        "-h", "--help", action="help",
        help="show this help message and exit")
    top_args.add_argument(
        "-v", "--verbose", action="store_true", dest="verbose",
        default=False, help = "more verbose printing of progress")
    top_args.add_argument(
        "-o", "--output",
        help="output directory")
    channel_args.add_argument(
        "-c", "--channel", default=[], action="append",
        help="single channel name (include a custom calibration factor by "+
        " joining with a comma, e.g. L1:IMC-WFS_A_I_YAW_OUT_DQ,-6.8e-3)")
    channel_args.add_argument(
        "-C", "--channel-list",
        help="*.txt file containing full channel names")
    channel_args.add_argument(
        "--station", type=lambda s: s.upper(), choices={"CS", "EX", "EY"},
        help="search for channels by station")
    channel_args.add_argument(
        "--search-channels",
        help="search for channels by name")
    plot_args.add_argument(
        "--freq-range", nargs=2, type=float,
        help="min and max frequencies for plot")
    plot_args.add_argument(
        "--cf-range", nargs=2, type=float,
        help="y-axis limits for coupling function plot")
    plot_args.add_argument(
        "--amb-range", nargs=2, type=float,
        help="y-axis limits for ambient plot")

    if script == 'pemcoupling':
        injection_args.add_argument(
            "-t", "--times", nargs=2,
            help="background and injection times (2 args)")
        injection_args.add_argument(
            "-d", "--dtt", nargs="+",
            help="scan DTT (.xml) file(s) for background/injection times")
        injection_args.add_argument(
            "-I", "--injection-list",
            help="*.txt file containing table of injection names, " +
            "background times, and injection times")
        injection_args.add_argument(
            "--search-injections",
            help="search for injections by name")
        fft_args.add_argument(
            "--fft-avg", type=int,
            help="number of FFT averages")
        fft_args.add_argument(
            "--duration", type=float,
            help="time series duration in seconds")
        fft_args.add_argument(
            "--bin-width", type=float,
            help="FFT bin width in Hz")
        fft_args.add_argument(
            "--overlap-pct", type=float,
            help="FFT overlap percentage (0-1.0)")
        analysis_args.add_argument(
            "--ignore-saturated", action="store_true", default=False,
            help="skip channels whose raw time series exceeds 32000 counts")
        analysis_args.add_argument(
            "--darm-threshold", type=float,
            help="minimum ratio of DARM injection ASD to background " +
            "ASD required to flag a coupling factor as a measured value")
        analysis_args.add_argument(
            "--sensor-threshold", type=float,
            help="minimum ratio of sensor injection ASD to background " +
            "ASD required to flag a coupling factor as a measured value " +
            "or upper limit")
        analysis_args.add_argument(
            "--sensor-threshold-autoscale", type=float,
            help="additional sensor threshold required for distinguishing " +
            "measurements and upper limits based on relative signal " +
            "(compared to channel that sees the largest signal)")
        analysis_args.add_argument(
            "--smoothing", nargs=2, type=float,
            help="apply smoothing to all channels (first value " +
            "applies to all ASDs, second value applies only to sensor " +
            "injection ASD)")
        analysis_args.add_argument(
            "--gate", action="store_true", default=False,
            help="gate out glitches in GW channel")
        analysis_args.add_argument(
            "--broadband-freqs", nargs=2, type=float,
            help="measure coupling in this frequency band only")
        analysis_args.add_argument(
            "--comb", nargs="?", type=float, const=True, default=False,
            help="measure coupling only near harmonics of this " +
            "frequency (frequency is inferred from injection name if " +
            "a value is not given here)")
        analysis_args.add_argument(
            "--line", nargs="?", type=float, const=True, default=False,
            help="measure coupling only near this frequency (frequency " +
            "is inferred from injection name if a value is not given here)")
        analysis_args.add_argument(
            "--slope-threshold", type=float,
            help="slope threshold for coupling function")
        plot_args.add_argument(
            "-r", "--ratio-plot", action="store_true", default=False,
            help="create ratio plot")
        plot_args.add_argument(
            "-R", "--ratio-plot-only", action="store_true", default=False,
            help="create ratio plot only; quit immediately after")
        postproc_args.add_argument(
            "-p", "--composite", action="store_true", default=False,
            help="compute composite coupling functions when finished " +
            "with single-injection coupling functions")
        postproc_args.add_argument(
            "-s", "--summary", action="store_true", default=False,
            help="generate summary plots when finished with composite " +
            "coupling functions (must be used with --composite)")
        top_args.add_argument(
            "--condor", action="store_true", default=False,
            help="split injections into condor jobs and submit each to " +
            "condor")

    if script == 'pemcoupling-composite':
        top_args.add_argument(
            "-d", "--directory",
            help="directory containing coupling function data")
        injection_args.add_argument(
            "--search-injections",
            help="search pattern for filtering injections by name")
        plot_args.add_argument(
            "-n","--no-upperlim", action="store_true", default=False,
            help="hide upper limits from plots")
        postproc_args.add_argument(
            "-s", "--summary", action="store_true", default=False,
            help="generate summary plots when finished")

    if script == 'pemcoupling-sitewide':
        top_args.add_argument(
            "-d", "--directory",
            help="directory containing composite coupling function data")
        analysis_args.add_argument(
            "-m", "--measured-value-threshold", type=float,
            help="minimum value allowed for max measured value as a " +
            "fraction of max upper limit")
        plot_args.add_argument(
            "-n","--no-upperlim", action="store_true", default=False,
            help="hide upper limits from plots")
        plot_args.add_argument(
            "--fig-size", nargs=2, type=float,
            help="custom figure width and height")
        plot_args.add_argument(
            "--plot-style", choices={'full', 'compact'}, default='full',
            help="plot style ('full' or 'compact')")
        plot_args.add_argument(
            "--cf-title", help="custom coupling function plot title")
        plot_args.add_argument(
            "--amb-title", help="custom estimated ambient plot title")
        plot_args.add_argument(
            "--basename", help="custom base name for all output files")

    if script == 'pemcoupling' and len(sys.argv) < 2:
        from . import interact
        interact_mode = interact.get_input("\nNo arguments given. Launch interactive mode (y/[n])?")
        if interact_mode.strip().lower() == 'y':
            return interact.interactive_session(script, parser)
        else:
            sys.exit()
    else:
        return parser.parse_args(args)


def get_channel_list(lines, station=None):
    channels = []
    calibration_factors = {}
    calibration_units = {}
    for line in lines:
        if len(line) == 0:
            continue
        split = line.strip().split()
        if len(split) > 0:
            name = split[0]
        else:
            continue
        calibration_factor, calibration_unit = (None, None)
        if len(split) > 1:
            try:
                calibration_factor = float(split[1])
            except (ValueError, TypeError):
                calibration_factor = 1
        if len(split) > 2:
            calibration_unit = split[2]
        if station in ['CS', 'EX', 'EY']:
            match_station = (channel.station == station)
        else:
            match_station = True
        if match_station:
            channel = PEMChannel(
                name,
                calibration_factor=calibration_factor,
                calibration_unit=calibration_unit)
            channels.append(channel)
    if len(channels) == 0:
        raise ValueError("No channels found in " + filename)
    return channels


def get_float(option, value):
    try:
        return float(value.strip())
    except (TypeError, ValueError):
        if option in FLOAT_REQUIRED:
            raise_input_err(
                "Float input required for option " + option)
        else:
            return None


def get_float_list(value):
    out = []
    for v in value.strip().split():
        try:
            out.append(float(v))
        except (TypeError, ValueError):
            pass
    if len(out) > 0:
        return out
    else:
        return None


def get_int(option, value):
    try:
        return int(value.strip())
    except (TypeError, ValueError):
        if option in INT_REQUIRED:
            raise_input_err(
                "Integer input required for option " + option)
        else:
            return None


def get_bool(value):
    return (value.lower() in ['on', 'true', 'yes'])


def get_smoothing_values(value):
    split = value.strip().split()
    if len(split) > 0:
        smooth_base = float(split[0])
        if len(split) > 1:
            smooth_inj = float(split[1])
        else:
            smooth_inj = smooth_base
    else:
        smooth_base, smooth_inj = (0, 0)
    return smooth_base, smooth_inj


def get_notches(lines):
    notch_list = []
    for line in lines:
        try:
            notch_freqs = [float(x) for x in line.split()]
        except ValueError:
            raise_input_err(
                "Config input for notches must be list of "
                "float pairs.")
        notch_list.append(tuple(notch_freqs))
    return notch_list


def get_plot_props(value):
    value = value.strip()
    if value == '':
        raise_input_err(
            "Input required for config option {}"
            .format(option))
    else:
        split = tuple(value.split())
        if len(split) == 1:
            split = (split[0], split[0])
        assert len(split) < 3,\
            "Up to two values allowed for " + option
        try:
            out = (float(split[0]), float(split[1]))
        except ValueError:
            out = tuple(split)
        return out


def parse_config(config_name):
    """
    Parse config file and return a dictionary of the config
    options and values.

    Parameters
    ----------
    config_name : str
        Name of config file.

    Returns
    -------
    config_dict : dict
        Dictionary containing sub-dictionaries of config
        options/values for every section in the config file.
    """

    # Read config file
    config_dir = os.path.dirname(config_name)
    config = ConfigParser.ConfigParser()
    logging.info("Opening config file.")
    try:
        config.read(config_name)
    except (IOError, OSError):
        raise_input_err(
            "Configuration file {} not found.".format(config_name),
            exc=OSError)
    # Check for missing/empty config file
    logging.info('Checking if missing/empty config file.')
    if len(config.sections()) == 0:
        raise_input_err(
            "Configuration file {} not found.".format(config_name),
            exc=OSError)
    elif all([len(config.options(x)) == 0 for x in config.sections()]):
        raise_input_err(
            "Configuration file {} is empty.".format(config_name))
    # Read config inputs into sub-dictionaries of config_dict
    # (separated by config section)
    config_dict = {s: dict(config.items(s)) for s in config.sections()}
    # Convert config options to appropriate types (float, int, bool, etc)
    logging.info('Parsing numerical and boolean config options.')
    for sub_dict in config_dict.values():
        for option, value in sub_dict.items():
            if option == 'channels':
                sub_dict[option] = get_channel_list(value.strip().split('\n'))
            elif option in FLOAT_OPTIONS or '_max' in option or '_min' in option:
                if option in ['band_freqs', 'line_freqs']:
                    sub_dict[option] = get_float_list(value)
                else:
                    sub_dict[option] = get_float(option, value)
            elif option in INT_OPTIONS:
                sub_dict[option] = get_int(option, value)
            elif option in BOOL_OPTIONS:
                sub_dict[option] = get_bool(value)
            elif option in FILENAME_OPTIONS:
                if value != '':
                    filename = os.path.join(config_dir, value)
                else:
                    filename = None
                sub_dict[option] = filename
            elif option in PLOT_OPTIONS:
                if value != '':
                    sub_dict[option] = get_plot_props(value)
                else:
                    raise_input_err(
                        "Input required for config option {}"
                        .format(option))
            elif option == 'smoothing':
                sub_dict[option] = get_smoothing_values(value)
            elif option == 'notches':
                lines = value.strip().split('\n')
                sub_dict[option] = get_notches(lines)
            else:
                sub_dict[option] = value
    return config_dict


def raise_input_err(err, exc=ValueError):
    print('')
    logging.error(err)
    print('')
    raise exc(err)
