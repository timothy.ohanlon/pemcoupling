#! /usr/bin/env python

import os
import glob
import sys
from pemcoupling.utils import get_times_from_DTT

try:
    files = sys.argv[1:]
except IndexError:
    files = glob.glob('*.xml')
injections = [os.path.splitext(os.path.basename(f))[0] for f in files]
times = [get_times_from_DTT(f) for f in files]
table = [(inj, str(t[0]), str(t[1])) for inj, t in zip(*(injections, times))]
for row in table:
    print(','.join(row))
