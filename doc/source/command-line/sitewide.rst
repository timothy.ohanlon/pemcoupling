.. _sitewide:

pemcoupling-sitewide
====================

A `site-wide or station-wide coupling function <../how-it-works/how-sitewide>`_
and estimated ambient can be produced via ::

    pemcoupling-sitewide config.ini ACC

This assumes that the
`composite coupling functions <../how-it-works/how-composite>`_ to be combined
into the summary plot exist in the directory provided in ``config.ini`` as
``composite_dir``. Otherwise, you
can use the ``-d`` or ``--directory`` option to point to the directory
containing composite coupling functions.

See :ref:`channels` for instructions on using a custom channel list or
searching for channel string patterns.

Full command-line help menu
---------------------------

.. program-output:: pemcoupling-sitewide --help