.. _usage-pemcoupling:

pemcoupling
===========

The primary function of pemcoupling is to compute coupling functions for
auxillary channels to the GW channel using data from PEM injections.

Contents:

.. contents:: :local:


Help overview
-------------

The general recipe for using ``pemcoupling`` is as follows:

1. Specify a **configuration file**.

2. Specify at least one analysis **section** of the configuration file to
process.

3. Specify **injection time(s)**.

4. Specify additional optional arguments.

This page will explain how to do each of these.

Below is information that can be found through the ``--help`` menu:

.. program-output:: pemcoupling --help


.. _config-file:

Specifying a config file
-----------------------------

Running ``pemcoupling`` always requires pointing to the desired configuration
file. Default config files are provided in the ``config_files/`` directory,
and can be copied and edited to your liking. See the
:ref:`Configuring pemcoupling<usage-config>` page for more information. Below
is an example of a config file for LHO.

.. raw:: html

    <a href="../_static/config.ini">Example configuration file</a>


.. _sections:

Specifying a configuration section
----------------------------------

The analysis portion of the configuration file is split into sections. In the
above example, the ``[MAG]`` section covers analysis options and channel info for
magnetometer channels, the ``[ACC]`` section for accelerometers, and so on. You
can make your own sections or modify the existing ones.

When running ``pemcoupling``, you must specify one or more section to use,
e.g. ::

    pemcoupling config.ini ACC MIC


.. _channels:

Specifying channels
-------------------

If no channel options (``--channel`` or ``--channel-list``) are provided, the
channel list(s) provided in the config file under the chosen section(s) will
be used.

The ``station`` option is usually desireable for only using channels located in
a particular station (``CS``, ``EX``, or ``EY``). For a more general method of
searching for channel names, use ``--search-channels``: ::

    # Process corner station channels provided in the ACC and MIC sections
    pemcoupling config.ini ACC MIC --station CS

    # Process HAM5 and HAM6 channels provided in the ACC and MIC sections
    pemcoupling config.ini ACC MIC --search-channels "HAM5 or HAM6"

Channel list
++++++++++++

A custom channel list can be provided in the config file or using the
channel-list command-line option: ::

    pemcoupling config.ini ACC MIC -C channel_list.txt

The ``channel_list.txt`` can consist of just a column of channel names,
but can also include a second column of calibration factors and a third column
of sensor units, in the same format as seen in channel lists in the example
config in :ref:`Specifying a config file<config-file>`. ::

    # Example of a channel list file **without** calibration info
    H1:PEM-CS_ACC_BEAMTUBE_MCTUBE_Y
    H1:PEM-CS_ACC_BEAMTUBE_SRTUBE_X
    H1:PEM-CS_ACC_BEAMTUBE_XMAN_Y
    H1:PEM-CS_ACC_BEAMTUBE_YMAN_X
    
    # Example of a channel list file **with** calibration info
    H1:PEM-CS_ACC_BEAMTUBE_MCTUBE_Y 5.9e-6 m/s2
    H1:PEM-CS_ACC_BEAMTUBE_SRTUBE_X 5.9e-6 m/s2
    H1:PEM-CS_ACC_BEAMTUBE_XMAN_Y 5.8e-6 m/s2
    H1:PEM-CS_ACC_BEAMTUBE_YMAN_X 7.59e-6 m/s2

Specific channels
+++++++++++++++++

Single channels can be provided individually via ::

    pemcoupling config.ini ACC -c H1:PEM-CS_ACC_PSL_TABLE_1_Z_DQ -c H1:PEM-CS_ACC_PSL_PERISCOPE_X_DQ

This would analyze the two PSL accelerometers over all the injections provided
in injection list. To use custom calibration factors for each channel, split
the channel and calibration factor with a comma (no space), e.g. ::

    pemcoupling config.ini WFS -c H1:IMC-WFS_A_DC_PIT_OUT,8.3e-2

Filtering channels by name
++++++++++++++++++++++++++

The user can search for a string pattern in the channel names: ::

    # Use config file channels that have "PSL" in their nmae
    pemcoupling config.ini ACC --search-channels PSL

or in a custom channel list file: ::

    # Use custom channel list channels that have "PSL" in their name
    pemcoupling config.ini ACC -C channel_list.txt --search-channels PSL

Filtering channels by location
++++++++++++++++++++++++++++++

Using the ``--station`` option is a useful shorthand for searching for
channels from a specific interferometer/station: ::

    # Use config file channels located at the Corner Station
    pemcoupling config.ini ACC --station CS


.. _injections:

Specifying injection times
--------------------------

There are many ways to provide ``pemcoupling`` with injection info, each
tailored to a different use case. Here is a summary of when you might want
to use each of these options:

+---------------------+-----------------------------------------------------+
| Method              | Usage                                               |
+=====================+=====================================================+
| -I (injection list) | You already have names and times recorded for all   |
|                     | the injections you want to analyze.                 |
+---------------------+-----------------------------------------------------+
| -t (single pair     | You've done one injection and just want to quickly  |
| of times)           | get some coupling functions done.                   |
+---------------------+-----------------------------------------------------+
| -d (DTT file or     | You just have DTT files of your injections.         |
| directory)          |                                                     |
+---------------------+-----------------------------------------------------+

Injection list
++++++++++++++

By default, ``pemcoupling`` looks for a list of injection names and times in
the configuration files. A list of injections can also be provided from the
command line with ::

    pemcoupling config.ini ACC -I injection_list.txt

Either way the list must be a file with three (comma-separated) columns, where
each row is an injection name, a background (quiet) GPS time, and an injection
GPS time, e.g. ::

    name_of_first_injection,1228718400,1228718500
    name_of_second_injection,1228718400,1228718600

Single background and injection time
++++++++++++++++++++++++++++++++++++

Specify just a single background (quiet) time and a single injection
(loud) time with ::

    pemcoupling config.ini ACC -t <background time> <injection time>

DTT file(s)
+++++++++++

When using DTT (Diagnostic Test Tool) files in .xml format, the name of each
DTT file becomes the name of the corresponding injection.

.. note::
    Be careful when using DTT files for injection times! The injection time is
    determined from the most recent measurement time in the DTT, and the
    background time is determined from the earliest reference time (unless
    it is the same as the injection time of a different injection in the same
    suite of DTTs, in which case the next most recent is chosen as background,
    and so on).

``pemcoupling`` can use the times found in one or more DTT files in .xml format: ::

    # Single DTT file
    pemcoupling config.ini ACC -d DTT_file.xml

    # Multiple DTT files
    pemcoupling config.ini ACC -d DTT_file1.xml DTT_file2.xml DTT_file3.xml

    # A DTT files in a directory
    pemcoupling config.ini ACC -d DTT_directory/*.xml

Searching injections
++++++++++++++++++++

If multiple injections are provided, whether by an injection list or by DTT
files, the user can search for a string pattern in the injection names, e.g.
searching for injections containing "acoustic", out of directory of DTT
files: ::

    pemcoupling config.ini ACC -d DTT_directory --search-injections acoustic


Full examples
-------------

Example #1
++++++++++
Suppose we've run one injection, to look at LLO PSL jitter via the WFS channels.
We can analyze the WFS channels with our own calibration factors at the times
we're interested in: ::

    pemcoupling config.ini WFS -c L1:IMC-WFS_A_I_PIT_OUT_DQ,2e-3 -c L1:IMC-WFS_A_I_YAW_OUT_DQ,6.8e-3 -t 1235356556 1235357462

Example #2
++++++++++
Suppose we've run a full suite of magnetic injections at LLO end X, and have
all the relevant DTT files saved in a directory ``LLO_EX_mag_DTTs``. We can
analyze all magnetometers there at once with the command ::

    pemcoupling config.ini MAG --station EX -d LLO_EX_mag_DTTs/* --composite

We could just as well use our own custom channel list, so we wouldn't have to
invoke the ifo, station, and injection_type: ::

    pemcoupling config.ini MAG -C my_favorite_LLO_EX_magnetometers.txt -d LLO_EX_mag_DTTs/* --composite

If we have the injection times already written to an injection table called
``some_injections.txt``, we can use that instead: ::

    pemcoupling config.ini MAG -C my_favorite_LLO_EX_magnetometers.txt -I some_injections.txt --composite



More options
------------

Comb injections
+++++++++++++++

By default ``pemcoupling`` computes broadband coupling functions. Use the
``--comb`` option to treat the injection(s) as comb injection(s). This will
compute coupling only at comb frequencies, taking the local maximum in
sensor amplitude around each frequency. If a frequency is provided, use
that as the fundamental. Otherwise, the fundamental frequency is inferred
from the name of the injection if possible. ::

    # Treat all injections as 7.1 Hz comb injections
    pemcoupling config.ini MAG -I injection_list.txt --comb 7.1

    # Infer the comb frequencies of each injection based on its name
    pemcoupling config.ini MAG -I injection_list.txt --comb

Coupling function thresholds
++++++++++++++++++++++++++++

These are provided in the config files, but the command-line options allow
you to override them for some quick tweaking: ::

    pemcoupling config.ini ACC MIC -I injection_list.txt --sensor-threshold 3 --darm-threshold 1.5

Smoothing parameters
++++++++++++++++++++

These are provided in the config files, but the command-line options allow
you to override them for some quick tweaking: ::

    pemcoupling config.ini ACC -I injection_list.txt --smoothing 5 0.5

The above choice of smoothing parameters would apply a spectrum smoothing
window of width 5 Hz (at 100 Hz) to the injection spectrum of the PEM
channel and 0.5 Hz (at 100 Hz) to the background spectrum of the PEM channel
and both spectra of the GW channel. See the page on
`Smoothing <../how-it-works/how-preprocess.html#smoothing>`_ for more information.

Amplitude ratio plots
+++++++++++++++++++++

A summary figure containing amplitude ratios for all channels during an
injection can be generated with ::

    pemcoupling config.ini ACC -C channel_list.txt -I injection_list.txt -r

This figure is useful for seeing the coverage of an injection without having
to look at the individual plots of all the channels analyzed. You can opt
to only generate a ratio plot (and not compute coupling functions) with the
``-R`` option instead.

Output directory
++++++++++++++++

If the ``-t`` option is used to compute coupling for a single background
time and injection time, the results are saved to a directory whose name is
the time at which ``pemcoupling`` was run. Otherwise, if DTT file(s) or an
injection list is used, each injection will have its own subdirectory, named
after the injection.

By default these subdirectories are saved to your current working directory.
Use the ``-o`` option to save to a different directory: ::

    pemcoupling config.ini ACC -C channel_list.txt -I injection_list.txt -o output_directory

Parallelization
+++++++++++++++

When dealing with massive channel lists, processing the injections in parallel
can come in handy. To exploit this, use the ``--condor`` toggle and each
injection will be submitted to Condor as a separate job: ::

    pemcoupling config.ini ACC -C channel_list.txt -I injection_list.txt --condor

If also using the ``--composite`` flag to generate composite coupling
functions, a ``pemcoupling-composite`` job will be run after all injections are
complete. A subdirectory "jobs/<current gps time>" will be created containing
the condor sub and dag files. Check on the progress of jobs with ``condor_q``
or via the log files in the ``jobs`` directory created in your current working
directory.