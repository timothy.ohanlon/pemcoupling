.. _how-composite:

Composite coupling functions
============================

Coupling functions from multiple injections can be combined for a single channel using `pemcoupling-composite <../usage/basic.html#composite>`_.
There are multiple methods to combine them. The example plots here are use the "highest amplitude injeciton" method described below.

.. image:: images/composite_coupling.png
    :width: 800

.. image:: images/composite_coupling_multi.png
    :width: 800

Highest amplitude injection
---------------------------

This method selects, at each frequency bin, the injection which had the highest sensor amplitude, i.e. the "loudest" injection.

Lowest coupling function
------------------------

This method selects, at each frequency bin, the injection with the lowest coupling function value.
